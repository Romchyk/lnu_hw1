/*eslint no-magic-numbers: ["error", { "ignore": [0, 1, 2] }]*/
function extractMiddle(str) {

    String.prototype.isEmpty = function () {
        return this.length === 0 || !this.trim();
    };

    if (str.isEmpty) {
        alert('Error');
        return false;
    }

    let position;
    let length;

    if (str.length % 2 === 1) {
        position = str.length / 2;
        length = 1;
    } else {
        position = str.length / 2 - 1;
        length = 2;
    }

    return str.substring(position, position + length)
}

let word = prompt('Input word: ');
alert(extractMiddle(word));
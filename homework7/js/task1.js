/*eslint no-magic-numbers: ["error", { "ignore": [100, 0, 10] }]*/
function counterTips() {
    let check = parseInt(prompt('Check number:'), 10);
    let tips = parseInt(prompt('Tip:', 10));

    if (!Number.isInteger(check) || !Number.isInteger(tips)) {
        alert('Must input numbers');
        return false;
    } else if (check < 0 || (0 > tips || tips > 100)) {
        alert('Error in input data');
        return false;
    }

    let tipAmount = check * tips / 100;
    let totalSum = check + tipAmount;

    alert(`
    Check number: ${check}
    Tip: ${tips}%
    Tip amount: ${Math.round(tipAmount*100) / 100}
    Total sum to pay: ${Math.round(totalSum*100) / 100}`);
}

counterTips();